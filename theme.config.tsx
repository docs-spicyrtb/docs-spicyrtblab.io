import React from 'react';
import { DocsThemeConfig } from 'nextra-theme-docs';
import Logo from './components/Logo';
import { Head } from './components/Head';
import { useNextSeoProps } from './config/useNextSeoProps';

const config: DocsThemeConfig = {
  logo: (
    <>
      <Logo />
      <span>SpicyDSP Documentation</span>
    </>
  ),
  editLink: {
    text: null,
  },
  feedback: {
    content: null,
  },
  head: Head,
  gitTimestamp: null,
  banner: {
    key: 'wip',
    text: '🚧 This documentation is a work in progress 🚧',
  },
  useNextSeoProps,
  i18n: [],
  footer: {
    text: '© 2023 SpicyDSP',
  },
};

export default config;
